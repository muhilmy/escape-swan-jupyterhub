FROM gitlab-registry.cern.ch/swan/docker-images/jupyterhub:v1.13

ADD ./lib /swan/lib

ADD ./lib/OIDCAuthenticator /tmp/OIDCAuthenticator
WORKDIR /tmp/OIDCAuthenticator
RUN pip install -r requirements.txt && \
    python3.6 setup.py install
WORKDIR /
